from PyQt5 import uic
from time import strftime
from datetime import datetime
import openpyxl
import sqlite3
import os
import docx
import win32com.client as win32
from PyQt5.QtWidgets import QApplication
from PyQt5.QtSql import QSqlQueryModel, QSqlDatabase


Form, Window = uic.loadUiType("main.ui")
app = QApplication([])
window = Window()
form = Form()
form.setupUi(window)
window.show()

global toreplace

maindict = {'OPERATOR': '',
            'SUBCON': '',
            'SUBCONBOSS': '',
            'CPSUBCON': '',
            'DATESTART': '',
            'DATEEND': '',
            'STREET': '',
            'NUM': '',
            'OKRUG': '',
            'RAYON': '',
            'UVD': '',
            'GIBDD': '',
            'UPRAVA': '',
            'FSO': ''}


def tablesfill():
    global maindict
    subconworkers = QSqlQueryModel()
    cherrypickers = QSqlQueryModel()
    subconcars = QSqlQueryModel()
    subconworkers.setQuery("SELECT "
                           "FIO AS 'Ф.И.О.', "
                           "BORNDATE AS 'Дата рождения', "
                           "PASSNUMBER AS 'Номер паспорта', "
                           "BORNPLACE AS 'Место рождения', "
                           "REGADDRESS AS 'Адрес регистрации', "
                           "NOTES AS 'Примечание' "
                           "FROM SubconWorkers "
                           "WHERE SUBCON = '{0}'".format(maindict['SUBCON']))
    cherrypickers.setQuery("SELECT "
                           "CAR AS 'Автовышка' "
                           "FROM SubconCars "
                           "WHERE CARTYPE = 'Автовышка' "
                           "AND SUBCON = '{0}'".format(maindict['CPSUBCON']))
    subconcars.setQuery("SELECT "
                        "CAR AS 'Автомобили прикрытия' "
                        "FROM SubconCars "
                        "WHERE CARTYPE = 'Автомобиль прикрытия' "
                        "AND SUBCON = '{0}'".format(maindict['SUBCON']))
    form.peopleTableView.setModel(subconworkers)
    form.peopleTableView.resizeColumnsToContents()
    form.cherrypickersTableView.setModel(cherrypickers)
    form.carsTableView.setModel(subconcars)


def excelimport():
    try:
        try:
            book = openpyxl.open('imports\\AdminDivision.xlsx', read_only=True)  # windows
        except:
            book = openpyxl.open('imports/AdminDivision.xlsx', read_only=True)  # linux
    except:
        print('Файл "AdminDivision.xlsx" не открылся, он должен быть в папке "imports".')
    else:
        try:
            sheet = book['административное деление']
        except:
            print('В файле "AdminDivision.xlsx" нет вкладки "административное деление".')
        else:
            cursor.execute('DELETE FROM AddrAndOrg')
            rowcounter = 4
            rowsamount = sheet.max_row
            print(f' [{strftime("%H:%M:%S")}]'
                  f' Загрузка со вкладки "административное деление" начата...')
            for row in sheet.iter_rows(min_row=4):
                rowcounter += 1
                if row[3].value is None:
                    nums = '(нет номеров)'
                else:
                    nums = str(row[3].value)
                for num in nums.split(', '):
                    print(f' [{strftime("%H:%M:%S")}]'
                          f' Cтрока: {rowcounter}/{rowsamount}. Дом: {num}.                             ', end='\r')
                    cursor.execute("INSERT INTO AddrAndOrg(STREET, NUM, OKRUG, RAYON, UVD, GIBDD, UPRAVA, FSO) "
                                   "VALUES(?, ?, ?, ?, ?, ?, ?, ?)",
                                   (row[2].value, num, row[0].value, row[1].value,
                                    row[4].value, row[6].value, row[5].value, row[7].value))
                con.commit()
            book.close()
            print(f' [{strftime("%H:%M:%S")}]'
                  f' Загрузка со вкладки "административное деление" завершена.                                    ')
            loadoperatorcombobox()


def importsubcons():
    try:
        try:
            book = openpyxl.open('imports\\SubconBossContacts.xlsx', read_only=True)  # windows
        except:
            book = openpyxl.open('imports/SubconBossContacts.xlsx', read_only=True)  # linux
    except:
        print('Файл "SubconBossContacts.xlsx" не открылся, он должен быть в папке "imports".')
    else:
        try:
            sheet = book['Контакты старших']
        except:
            print('В файле "SubconBossContacts.xlsx" нет вкладки "Контакты старших".')
        else:
            cursor.execute('DELETE FROM SubconContact')
            rowcounter = 2
            rowsamount = sheet.max_row
            print(f' [{strftime("%H:%M:%S")}]'
                  f' Загрузка подрядчиков со вкладки "Контакты старших" начата...')
            for row in sheet.iter_rows(min_row=2):
                print(f' [{strftime("%H:%M:%S")}]'
                      f' Cтрока: {rowcounter}/{rowsamount}.                                             ', end='\r')
                rowcounter += 1
                if row[0].value is not None:
                    cursor.execute("INSERT INTO SubconContact(SUBCON, SUBCONBOSS) "
                                   "VALUES(?, ?)",
                                   (row[0].value, row[1].value))
                con.commit()
            book.close()
            print(f' [{strftime("%H:%M:%S")}]'
                  f' Загрузка подрядчиков со вкладки "Контакты старших" завершена.                                ')
    reloadsubconsfromdb()


def importcars():
    cursor.execute('DELETE FROM SubconCars')
    try:
        book = openpyxl.open('imports\\Автомобили\\Автовышки.xlsx', read_only=True)
        sheet = book.active
    except:
        print(f' [{strftime("%H:%M:%S")}] Файл "imports\\Автомобили\\Автовышки.xlsx" не открылся')
    else:
        rowcounter = 1
        rowsamount = sheet.max_row
        print(f' [{strftime("%H:%M:%S")}] Загрузка автовышек начата...')
        for row in sheet.iter_rows(min_row=2, values_only=True):
            rowcounter += 1
            if row[1] is not None:
                print(f' Загружено: [{rowcounter}/{rowsamount}]', end='\r')
                cursor.execute("INSERT INTO SubconCars(SUBCON, CAR, CARTYPE) "
                               "VALUES(?, ?, ?)", (row[0], row[1], 'Автовышка'))
        con.commit()
        book.close()
        print(f' [{strftime("%H:%M:%S")}] Загрузка автовышек завершена.                 ')

    try:
        book = openpyxl.open('imports\\Автомобили\\Автомобили прикрытия.xlsx', read_only=True)
        sheet = book.active
    except:
        print(f' [{strftime("%H:%M:%S")}] Файл "imports\\Автомобили\\Автомобили прикрытия.xlsx" не открылся')
    else:
        rowcounter = 1
        rowsamount = sheet.max_row
        print(f' [{strftime("%H:%M:%S")}] Загрузка автомобилей прикрытия начата...')
        for row in sheet.iter_rows(min_row=2, values_only=True):
            rowcounter += 1
            if row[1] is not None:
                print(f' Загружено: [{rowcounter}/{rowsamount}]', end='\r')
                cursor.execute("INSERT INTO SubconCars(SUBCON, CAR, CARTYPE) "
                               "VALUES(?, ?, ?)", (row[0], row[1], 'Автомобиль прикрытия'))
        con.commit()
        book.close()
        print(f' [{strftime("%H:%M:%S")}] Загрузка автомобилей прикрытия завершена.                 ')
    reloadcherrypickersubconsfromdb()


def importworkers():
    cursor.execute('DELETE FROM SubconWorkers')
    subconworkerfiles = []
    for dirpath, dirnames, filenames in os.walk("imports\\Сотрудники"):
        for filename in filenames:
            if filename.split('.')[-1] in ['xlsx', 'xls', 'XLSX', 'XLS']:
                subconworkerfiles.append(str(''.join(list(os.path.join(dirpath, filename)))))
    filesqty = len(subconworkerfiles)
    filenum = 0
    print(f' {strftime("%H:%M:%S")} Загрузка сотрудников начата...')
    excel = win32.gencache.EnsureDispatch('Excel.Application')
    for file in subconworkerfiles:
        if file.split('.')[-1] in ['xls', 'XLS']:
            fileabs = os.path.abspath(file)
            book = excel.Workbooks.Open(fileabs)
            fileabs = fileabs[:-4] + '.xlsx'
            filedel = file
            file = file[:-4] + '.xlsx'
            book.SaveAs(fileabs, FileFormat=51)  # FileFormat = 51 is for .xlsx extension
            book.Close()
            os.remove(filedel)
        filenum += 1
        subcontitle = file.split('\\')[-1][19:-5]
        print(strftime(" %H:%M:%S"), f'[{filenum}/{filesqty}] {subcontitle}')
        try:
            book = openpyxl.open(file, read_only=True)
            sheet = book.active
        except:
            print(f' [{strftime("%H:%M:%S")}] Файл от {subcontitle} не открылся')
        else:
            rowcounter = 0
            for row in sheet.iter_rows(max_col=9, values_only=True):
                try:
                    datestring = row[3].strftime('%d.%m.%Y')
                except AttributeError:
                    datestring = row[3]
                finally:
                    rowcounter += 1
                    if row[0] is not None:
                        cursor.execute("INSERT INTO SubconWorkers("
                                       "SUBCON, "
                                       "FIO, "
                                       "BORNDATE, "
                                       "PASSNUMBER, "
                                       "REGADDRESS, "
                                       "BORNPLACE, "
                                       "NOTES) "
                                       "VALUES(?, ?, ?, ?, ?, ?, ?)",
                                       (subcontitle,
                                        row[0] + ' ' + row[1] + ((' ' + row[2]) if row[2] is not None else ''),
                                        datestring,
                                        row[5] if row[4] is None else f'{row[4]} {row[5]}',
                                        row[7],
                                        row[6],
                                        row[8]))
            con.commit()
            book.close()
    print(f' {strftime("%H:%M:%S")} Загрузка сотрудников завершена.                 ')


db = QSqlDatabase.addDatabase('QSQLITE')
db.setDatabaseName("gn.db")
db.open()
con = sqlite3.connect('gn.db')
cursor = con.cursor()
cursor.execute("PRAGMA main.auto_vacuum = 1")
cursor.execute("CREATE TABLE IF NOT EXISTS AddrAndOrg("
               "id integer PRIMARY KEY, "
               "STREET text, "
               "NUM text,"
               "OKRUG text, "
               "RAYON text, "
               "UVD text, "
               "GIBDD text, "
               "UPRAVA text, "
               "FSO text)")
cursor.execute("CREATE TABLE IF NOT EXISTS GibddContact("
               "GIBDD text PRIMARY KEY, "
               "CONTACTS text)")
cursor.execute("CREATE TABLE IF NOT EXISTS GibddBossTerritory("
               "GIBDD text PRIMARY KEY, "
               "BOSS text, "
               "TERRITORY text)")
cursor.execute("CREATE TABLE IF NOT EXISTS SubconContact("
               "SUBCON text PRIMARY KEY, "
               "SUBCONBOSS text)")
cursor.execute("CREATE TABLE IF NOT EXISTS SubconWorkers("
               "SUBCON text, "
               "FIO text, "
               "BORNDATE text, "
               "PASSNUMBER text, "
               "REGADDRESS text, "
               "BORNPLACE text, "
               "NOTES text)")
cursor.execute("CREATE TABLE IF NOT EXISTS SubconCars("
               "SUBCON text, "
               "CAR text, "
               "CARTYPE text)")
con.commit()

# cursor.execute("INSERT INTO SubconWorkers(SUBCON, FIO, CITIZ, PASSID, PASSISSUE, JOBTITLE) VALUES(?, ?, ?, ?, ?, ?)",
#                ('ИП Сидоров',
#                 'Смирнов Станислав Александрович',
#                 'РФ',
#                 '33 11 081763',
#                 'ОТДЕЛЕНИЕМ УФМС РОССИИ ПО КИРОВСКОЙ ОБЛАСТИ В ГОРОДЕ КОТЕЛЬНИЧЕ Дата выдачи: 01.07.2011 '
#                 'Код подразделения: 432-013 Дата рождения: 15.05.1991',
#                 'Монтажник'))
# cursor.execute("INSERT INTO SubconCars(SUBCON, CAR, CARTYPE) VALUES(?, ?, ?)",
#                ('ИП Сидоров',
#                 'Мерседес спринтер К294ХА799',
#                 'Машина прикрытия'))
# cursor.execute("INSERT INTO SubconCars(SUBCON, CAR, CARTYPE) VALUES(?, ?, ?)",
#                ('ИП Сидоров',
#                 'JAC 120  —  С739КУ 799',
#                 'Автовышка'))
# con.commit()


def labelsclear():
    form.uvdlabel.setText('УВД: не определено')
    form.gibddlabel.setText('ГИБДД: не определено')
    form.upravalabel.setText('Управа: не определено')
    maindict['UVD'] = ''
    maindict['GIBDD'] = ''
    maindict['UPRAVA'] = ''
    maindict['FSO'] = ''


form.dateEditStart.setDate(datetime.now())
form.dateEditEnd.setDate(datetime.now())


def loadoperatorcombobox():
    form.comboBoxOperator.addItems(['выбрать...', 'ПАО «ВымпелКом»', 'ПАО «МТС»'])
    form.comboBoxOkrug.clear()
    okrugs = ['выбрать...']
    okrugstups = cursor.execute('SELECT DISTINCT OKRUG FROM AddrAndOrg ORDER BY OKRUG').fetchall()
    for okrugtup in okrugstups:
        okrugs.append(okrugtup[0])
    form.comboBoxOkrug.addItems(okrugs)


loadoperatorcombobox()


def reloadsubconsfromdb():
    maindict['SUBCON'] = ''
    maindict['SUBCONBOSS'] = ''
    form.subconBossEdit.text = ''
    form.comboBoxSubcon.clear()
    subcons = ['выбрать...']
    subconstups = cursor.execute('SELECT SUBCON FROM SubconContact ORDER BY SUBCON').fetchall()
    for subcontup in subconstups:
        subcons.append(subcontup[0])
    form.comboBoxSubcon.addItems(subcons)


reloadsubconsfromdb()


def reloadcherrypickersubconsfromdb():
    maindict['CPSUBCON'] = ''
    form.comboBoxCPSubcon.clear()
    cpsubcons = ['выбрать...']
    cpsubconstups = cursor.execute('SELECT DISTINCT SUBCON '
                                   'FROM SubconCars '
                                   'WHERE CARTYPE = "Автовышка" '
                                   'ORDER BY SUBCON').fetchall()
    for cpsubcontup in cpsubconstups:
        cpsubcons.append(cpsubcontup[0])
    form.comboBoxCPSubcon.addItems(cpsubcons)


reloadcherrypickersubconsfromdb()


def afteroperatorchanged():
    maindict['OPERATOR'] = form.comboBoxOperator.currentText()
    printselection()


def afterokrugchanged():
    maindict['OKRUG'] = form.comboBoxOkrug.currentText()
    if form.comboBoxOkrug.currentText() != 'выбрать...':
        form.comboBoxRayon.clear()
        form.comboBoxStreet.clear()
        form.comboBoxNum.clear()
        rayons = ['выбрать...']
        rayonstups = cursor.execute('SELECT DISTINCT RAYON FROM AddrAndOrg '
                                    'WHERE OKRUG = ? ORDER BY RAYON',
                                    (form.comboBoxOkrug.currentText(), )).fetchall()
        for rayontup in rayonstups:
            rayons.append(rayontup[0])
        form.comboBoxRayon.addItems(rayons)
        labelsclear()
        printselection()


def afterrayonchanged():
    maindict['RAYON'] = form.comboBoxRayon.currentText()
    if form.comboBoxRayon.currentText() != 'выбрать...':
        form.comboBoxStreet.clear()
        form.comboBoxNum.clear()
        streets = ['выбрать...']
        streetstups = cursor.execute('SELECT DISTINCT STREET FROM AddrAndOrg '
                                     'WHERE OKRUG = ? AND RAYON = ? ORDER BY STREET',
                                     (form.comboBoxOkrug.currentText(),
                                      form.comboBoxRayon.currentText())).fetchall()
        for streettup in streetstups:
            streets.append(streettup[0])
        form.comboBoxStreet.addItems(streets)
        labelsclear()
        printselection()


def afterstreetchanged():
    maindict['STREET'] = form.comboBoxStreet.currentText()
    if form.comboBoxStreet.currentText() != 'выбрать...':
        form.comboBoxNum.clear()
        nums = ['выбрать...']
        numstups = cursor.execute('SELECT DISTINCT NUM FROM AddrAndOrg '
                                  'WHERE OKRUG = ? AND RAYON = ? AND STREET = ?',
                                  (form.comboBoxOkrug.currentText(),
                                   form.comboBoxRayon.currentText(),
                                   form.comboBoxStreet.currentText())).fetchall()
        for numtup in numstups:
            nums.append(numtup[0])
        form.comboBoxNum.addItems(nums)
        labelsclear()
        printselection()


def afternumchanged():
    maindict['NUM'] = form.comboBoxNum.currentText()
    organs = cursor.execute('SELECT UVD, GIBDD, UPRAVA, FSO FROM AddrAndOrg '
                            'WHERE OKRUG = ? AND RAYON = ? AND STREET = ? AND NUM = ?',
                            (form.comboBoxOkrug.currentText(),
                             form.comboBoxRayon.currentText(),
                             form.comboBoxStreet.currentText(),
                             form.comboBoxNum.currentText())).fetchone()
    if organs is not None:
        form.uvdlabel.setText('УВД: ' + organs[0])
        form.gibddlabel.setText('ГИБДД: ' + organs[1])
        form.upravalabel.setText('Управа: ' + organs[2])
        maindict['UVD'] = organs[0]
        maindict['GIBDD'] = organs[1]
        maindict['UPRAVA'] = organs[2]
        maindict['FSO'] = organs[3]
    printselection()


def afterdatesedit():
    maindict['DATESTART'] = form.dateEditStart.date().toString("dd.MM.yyyy")
    maindict['DATEEND'] = form.dateEditEnd.date().toString("dd.MM.yyyy")
    printselection()


def aftersubconchanged():
    try:
        form.subconBossEdit.setText(cursor.execute('SELECT SUBCONBOSS FROM SubconContact WHERE SUBCON = ?',
                                                   (form.comboBoxSubcon.currentText(), )).fetchone()[0])
    except:
        form.subconBossEdit.setText('')
        maindict['SUBCON'] = ''
        printselection()
    else:
        maindict['SUBCON'] = form.comboBoxSubcon.currentText()
        printselection()
        tablesfill()


def aftercpsubconchanged():
    try:
        maindict['CPSUBCON'] = form.comboBoxCPSubcon.currentText()
    except:
        maindict['CPSUBCON'] = ''
    else:
        tablesfill()
    finally:
        printselection()


def aftersubconbossedit(text):
    maindict['SUBCONBOSS'] = text
    printselection()


def printselection():
    try:
        os.system('cls')  # windows
    except:
        os.system('clear')  # linux

    print('Ключ замены      Значение замены\n',
          '[OPERATOR]      ', maindict['OPERATOR'], '\n',
          '[OKRUG]         ', maindict['OKRUG'], '\n',
          '[RAYON]         ', maindict['RAYON'], '\n',
          '[STREET]        ', maindict['STREET'], '\n',
          '[NUM]           ', maindict['NUM'], '\n',
          '[GIBDD]         ', maindict['GIBDD'], '\n',
          '[UVD]           ', maindict['UVD'], '\n',
          '[UPRAVA]        ', maindict['UPRAVA'], '\n',
          '[DATESTART]     ', maindict['DATESTART'], '\n',
          '[DATEEND]       ', maindict['DATEEND'], '\n',
          '[SUBCON]        ', maindict['SUBCON'], '\n',
          '[SUBCONBOSS]    ', maindict['SUBCONBOSS'], '\n')


def completereplacements():
    global toreplace
    toreplace = {'[GIBDD]': maindict['GIBDD'],
                 '[UVD]': maindict['UVD'],
                 '[UPRAVA]': maindict['UPRAVA'],
                 '[OPERATOR]': maindict['OPERATOR'],
                 '[OKRUG]': maindict['OKRUG'],
                 '[RAYON]': maindict['RAYON'],
                 '[STREET]': maindict['STREET'],
                 '[NUM]': maindict['NUM'],
                 '[SUBCON]': maindict['SUBCON'],
                 '[DATESTART]': maindict['DATESTART'],
                 '[DATEEND]': maindict['DATEEND'],
                 '[TODAY]': str(datetime.now().strftime('%d.%m.%Y')),
                 '[SUBCONBOSS]': maindict['SUBCONBOSS']}


def carsandworkerstablecompletion(doc):
    for cartype in ['Автомобиль прикрытия', 'Автовышка']:
        carslist = cursor.execute('SELECT CAR FROM SubconCars '
                                  'WHERE SUBCON = ? AND CARTYPE = ?',
                                  (maindict[('CPSUBCON' if cartype == 'Автовышка' else 'SUBCON')], cartype)).fetchall()
        carsstring = ''
        for cartup in carslist:
            carsstring = carsstring + ('\n' if carsstring != '' else '') + cartup[0]
        doc.tables[0].cell(1, (0 if cartype == 'Автовышка' else 1)).text = carsstring

    workerslist = cursor.execute('SELECT FIO, BORNDATE, PASSNUMBER, REGADDRESS, NOTES '
                                 'FROM SubconWorkers '
                                 'WHERE SUBCON = ?',
                                 (maindict['SUBCON'], )).fetchall()
    workernum = 0
    for workertup in workerslist:
        workernum += 1
        doc.tables[1].add_row()
        column = 0
        for workercell in workertup:
            doc.tables[1].cell(workernum, column).text = (workercell if workercell is not None else '')
            column += 1


def pushuvd():
    global toreplace
    doc = docx.Document('templates\\УВД.docx')
    carsandworkerstablecompletion(doc)
    completereplacements()
    for i in toreplace:
        for p in doc.paragraphs:
            if p.text.find(i) >= 0:
                p.text = p.text.replace(i, toreplace[i])
    doc.save(f"Готовые письма\\УВД ({maindict['STREET']}) [{datetime.now().strftime('%Y.%m.%d %H-%M-%S')}].docx")
    print('\nПисьмо для УВД готово.')


def pushgibdd():
    global toreplace
    doc = docx.Document('templates\\ГИБДД.docx')
    carsandworkerstablecompletion(doc)
    completereplacements()
    for i in toreplace:
        for p in doc.paragraphs:
            if p.text.find(i) >= 0:
                p.text = p.text.replace(i, toreplace[i])
    doc.save(f"Готовые письма\\ГИБДД ({maindict['STREET']}) [{datetime.now().strftime('%Y.%m.%d %H-%M-%S')}].docx")
    print('\nПисьмо для ГИБДД готово.')


def pushuprava():
    global toreplace
    doc = docx.Document('templates\\УПРАВА.docx')
    carsandworkerstablecompletion(doc)
    completereplacements()
    for i in toreplace:
        for p in doc.paragraphs:
            if p.text.find(i) >= 0:
                p.text = p.text.replace(i, toreplace[i])
    doc.save(f"Готовые письма\\Управа ({maindict['STREET']}) [{datetime.now().strftime('%Y.%m.%d %H-%M-%S')}].docx")
    print('\nПисьмо для управы готово.')


form.action_AdmDiv.triggered.connect(excelimport)
form.action_importsubcons.triggered.connect(importsubcons)
form.action_importworkers.triggered.connect(importworkers)
form.action_importcars.triggered.connect(importcars)
form.comboBoxOperator.currentTextChanged.connect(afteroperatorchanged)
form.comboBoxOkrug.currentTextChanged.connect(afterokrugchanged)
form.comboBoxRayon.currentTextChanged.connect(afterrayonchanged)
form.comboBoxStreet.currentTextChanged.connect(afterstreetchanged)
form.comboBoxNum.currentTextChanged.connect(afternumchanged)
form.comboBoxSubcon.currentTextChanged.connect(aftersubconchanged)
form.comboBoxCPSubcon.currentTextChanged.connect(aftercpsubconchanged)
form.subconBossEdit.textChanged[str].connect(aftersubconbossedit)
form.dateEditStart.userDateChanged.connect(afterdatesedit)
form.dateEditEnd.userDateChanged.connect(afterdatesedit)
form.pushUVD.clicked.connect(pushuvd)
form.pushGIBDD.clicked.connect(pushgibdd)
form.pushUPRAVA.clicked.connect(pushuprava)


app.exec()
